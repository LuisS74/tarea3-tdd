package cl.ufro.dci.tdd.model;

import lombok.Getter;
import lombok.Setter;

import java.util.regex.Pattern;

@Getter
@Setter
public class Usuario {
    private String nombre;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String rut;
    private int numeroTelefonico;
    private int edad;

    public Usuario(){
    }

    public boolean validarNombre(String usuario) {
        String regex = ".*\\d.*";
        return !Pattern.compile(regex).matcher(usuario).matches();
    }

    public boolean validarRut(String rut) {
        boolean validacion = false;

        try {
            rut = rut.toUpperCase();
            rut = rut.replace(".", "");
            rut = rut.replace("-", "");
            int rutAux = Integer.parseInt(rut.substring(0, rut.length() - 1));

            char dv = rut.charAt(rut.length() - 1);

            int m = 0, s = 1;
            for (; rutAux != 0; rutAux /= 10) {
                s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
            }

            if (dv == (char) (s != 0 ? s + 47 : 75)) {
                validacion = true;
            }
        } catch (java.lang.NumberFormatException e) {
        } catch (Exception e) {
        }

        return validacion;
    }

    public boolean validarTelefono(int numeroTelefonico) {
        String telefono = String.valueOf(numeroTelefonico);
        if (telefono.length() == 9) {
            return true;
        } else {
            return false;
        }
    }

    public boolean validarEdad(int edad) {
        return edad >= 0;
    }
}
