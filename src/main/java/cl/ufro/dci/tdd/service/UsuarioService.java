package cl.ufro.dci.tdd.service;

import cl.ufro.dci.tdd.model.Usuario;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.regex.Pattern;

@Service
public class UsuarioService {
    public ResponseEntity<String> crearUsuario(Usuario usuario) {
        if (usuario.getNombre() != null && !usuario.validarNombre(usuario.getNombre())) {
            return ResponseEntity.badRequest().body("El nombre contiene números");
        } else if (usuario.getApellidoPaterno() != null && !usuario.validarNombre(usuario.getApellidoPaterno())) {
            return ResponseEntity.badRequest().body("El apellido paterno contiene números");
        } else if (usuario.getApellidoMaterno() != null && !usuario.validarNombre(usuario.getApellidoMaterno())) {
            return ResponseEntity.badRequest().body("El apellido materno contiene números");
        } else if (usuario.getRut() != null && !usuario.validarRut(usuario.getRut())) {
            return ResponseEntity.badRequest().body("Rut invalido");
        } else if (!usuario.validarTelefono(usuario.getNumeroTelefonico())) {
            return ResponseEntity.badRequest().body("El número de telefono esta incorrecto");
        }else if (!usuario.validarEdad(usuario.getEdad())) {
            return ResponseEntity.badRequest().body("La edad no corresponde");
        }

        return ResponseEntity.status(HttpStatus.CREATED).body("Usuario ingresado correctamente!");
    }
}
