package cl.ufro.dci.tdd.model;

import org.junit.jupiter.api.*;

class UsuarioTest {
    Usuario usuario;

    @BeforeEach
    void setUp() {
        usuario = new Usuario();
    }

    @Test
    @DisplayName("Test nombre no debe contener números")
    public void testValidarNombre() {
        usuario.setNombre("John");
        Assertions.assertTrue(usuario.validarNombre(usuario.getNombre()));
    }

    @Test
    @DisplayName("Test apellido paterno no debe contener números")
    public void testValidarApellidoPaterno() {
        usuario.setApellidoPaterno("Sandoval");
        Assertions.assertTrue(usuario.validarNombre(usuario.getApellidoPaterno()));
    }

    @Test
    @DisplayName("Test apellido materno no debe contener números")
    public void testValidarApellidoMaterno() {
        usuario.setApellidoMaterno("Pino");
        Assertions.assertTrue(usuario.validarNombre(usuario.getApellidoMaterno()));
    }

    @Test
    @DisplayName("Test rut de usuario")
    public void testRut(){
        usuario.setRut("21.158.172-7");
        Assertions.assertTrue(usuario.validarRut(usuario.getRut()));
    }

    @Test
    @DisplayName("Test numero telefonico de usuario ")
    public void testTelefono(){
        usuario.setNumeroTelefonico(123456789);
        Assertions.assertTrue(usuario.validarTelefono(usuario.getNumeroTelefonico()));
    }

    @Test
    @DisplayName("Test edad no debe ser numero negativo")
    public void testEdad(){
        usuario.setEdad(30);
        Assertions.assertTrue(usuario.validarEdad(usuario.getEdad()));
    }

    @AfterEach
    void tearDown() {
    }
}